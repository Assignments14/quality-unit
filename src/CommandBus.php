<?php

namespace Rooslunn\QUA;

use Rooslunn\QUA\Contracts\Command;
use Rooslunn\QUA\Contracts\Storage;

final class CommandBus
{

    private Storage $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage; 
    }

    public function execute(array $commands): array
    {
        $output = [];
        foreach ($commands as $command) {
            $parsedCommand = $this->parseCommand($command);
            if ($parsedCommand && ($result = $parsedCommand->execute($this->storage))) {
                $output[] = $result;
            }
        }
        return $output;
    }

    private function parseCommand(string $command): Command
    {
        return CommandFactory::command($command);
    }
}