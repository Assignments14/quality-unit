<?php

namespace Rooslunn\QUA\Commands;


abstract class BaseCommand
{
    protected array $fields;

    public function __construct(array $fields)
    {
        $this->fields = $fields; 
    }

    public function getField(string $name): ?string
    {
        return array_key_exists($name, $this->fields) ? $this->fields[$name]: null;
    }

    public function fields(): array
    {
        return $this->fields; 
    }

}