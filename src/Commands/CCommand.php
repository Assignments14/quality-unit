<?php

namespace Rooslunn\QUA\Commands;

use Rooslunn\QUA\Commands\BaseCommand;
use Rooslunn\QUA\Contracts\Command;
use Rooslunn\QUA\Contracts\Storage;

final class CCommand extends BaseCommand implements Command
{
    private function convertToTypes()
    {
        if (isset($this->fields['date'])) {
            $this->fields['date'] = ymdString($this->fields['date']);
        }
    }        

    public function execute(Storage $storage): ?string
    {
        $this->convertToTypes();
        $storage->insert($this->fields());
        return null;
    }
}