<?php

namespace Rooslunn\QUA\Commands;

use Rooslunn\QUA\Commands\BaseCommand;
use Rooslunn\QUA\Contracts\Command;
use Rooslunn\QUA\Contracts\Storage;

class DCommand extends BaseCommand implements Command
{
    public const PERIOD_SEPARATOR = '-';

    private function isRealPeriod(string $period): bool
    {
        return strpos($period, self::PERIOD_SEPARATOR) > 0;
    }

    private function parsePeriod(): void
    {
        if (isset($this->fields['period'])) {
            $period = $this->fields['period'];
            if (!$this->isRealPeriod($period)) {
                $period .= self::PERIOD_SEPARATOR . $period;
            }
            [$from, $to] = explode(self::PERIOD_SEPARATOR, $period);
            $from = ymdString($from, '00:00:00');
            $to = ymdString($to, '23:59:59');
            $this->fields += compact('from', 'to');
        }
    }

    public function execute(Storage $storage): ?string
    {
        $this->parsePeriod();

        $sql = 
            'select sum(cast(time as integer))/count(time) as avg_time from commands
            where 
                type = "C" 
                and (service like :service_like or :service_origin = "*")
                and (question like :question_like or :question_origin = "*")
                and response = :response
                and datetime(date) between datetime(:from) and datetime(:to)';

        $row = $storage->query($sql)
            ->bind(':service_like', likeClause($this->fields['service']))
            ->bind(':service_origin', $this->fields['service'])
            ->bind(':question_like', likeClause($this->fields['question']))
            ->bind(':question_origin', $this->fields['question'])
            ->bind(':response', $this->fields['response'])
            ->bind(':from', $this->fields['from'])
            ->bind(':to', $this->fields['to'])
            ->fetchOne();

        return $row->avg_time ?? '-';
    }
}