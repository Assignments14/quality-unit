<?php

namespace Rooslunn\QUA\Types;

class CommandType
{
    public const C_COMMAND = 'C';
    public const D_COMMAND = 'D';
}