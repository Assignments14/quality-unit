<?php

namespace Rooslunn\QUA;

use Rooslunn\QUA\Exceptions\BadCommand;
use Rooslunn\QUA\Contracts\Command;
use Rooslunn\QUA\Types\CommandType; 
use Rooslunn\QUA\Commands\{
    CCommand, DCommand
};

class CommandFactory
{
    public const COMMAND_PARTS_SEPARATOR = ' ';

    protected string $rawCommand;

    protected array $structure = [
        CommandType::C_COMMAND => ['type', 'service', 'question', 'response', 'date', 'time'],
        CommandType::D_COMMAND => ['type', 'service', 'question', 'response', 'period'],
    ];

    public function __construct(string $command)
    {
        $this->rawCommand = $command; 
    }

    protected function getType(array $parts): string
    {
        if (! isset($parts[0])) {
            throw new BadCommand('Command Type index (0) is not set');
        }         

        return trim($parts[0]);
    }

    protected function chooseStructure(string $commandType): array
    {
        if (! array_key_exists($commandType, $this->structure)) {
            throw new  BadCommand('Command structure undefined');
        }
        return $this->structure[$commandType];
    }

    protected function dissect(): array
    {
        $parts = explode(self::COMMAND_PARTS_SEPARATOR, $this->rawCommand);
        $commandType = $this->getType($parts); 
        $structure = $this->chooseStructure($commandType);

        $fields = [];

        foreach ($structure as $key => $field) {
            $fields[$field] = $parts[$key];
        }

        return $fields;
    }

    public static function command(string $command): ?Command
    {
        $cmd = new self($command);
        $fields = $cmd->dissect();

        if ($fields['type'] === CommandType::C_COMMAND) {
            return new CCommand($fields);
        }

        if ($fields['type'] === CommandType::D_COMMAND) {
            return new DCommand($fields);
        }

        return null;
    }
}