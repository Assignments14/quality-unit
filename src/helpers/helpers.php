<?php

if (! function_exists('ymdString')) {
    function ymdString(string $date, string $time = '00:00:00'): string {
        $datetime = sprintf('%s %s', $date, $time);
        return date('Y-m-d', strtotime($datetime));
    }
}

if (! function_exists('likeClause')) {
    function likeClause(string $clause): string {
        return $clause . '%';
    }
}