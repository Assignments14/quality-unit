<?php

namespace Rooslunn\QUA\Contracts;

use stdClass;

interface Storage
{
    public function bind(string $param, $value, int $type = null): self;
    public function insert(array $data): bool;
    public function query(string $sql): self;
    public function fetchAll(): array;
    public function fetchOne(): ?stdClass;
}