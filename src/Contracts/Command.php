<?php

namespace Rooslunn\QUA\Contracts;

use Rooslunn\QUA\Contracts\Storage;

interface Command
{
    public function execute(Storage $storage): ?string;
}